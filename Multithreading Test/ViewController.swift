//
//  ViewController.swift
//  Multithreading Test
//
//  Created by Gabo Lugo on 4/7/16.
//  Copyright © 2016 Inspectezr. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController {
    
    var mainList = [Section]()
    
    // Put in a sctruct or something more elegant
    var currentUrl = ""
    var currentName = ""
    
    
    @IBOutlet weak var stageTable: UITableView!
    @IBOutlet weak var loadingOverlay: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.networkStatusChanged(_:)), name: ReachabilityStatusChangedNotification, object: nil)
        Reach().monitorReachabilityChanges()
        
        print("Init downloading data from API.")
        
        
        loadingOverlay.hidden = false
        
        Alamofire.request(.GET, "http://private-3a5e8-photos7.apiary-mock.com/photos", parameters: nil).responseJSON { response in
            
            switch response.result {
            
            case .Success(let data):
                let json = JSON(data)
                
                for item in json.arrayValue {
                    
                    let newItem = Section(json: item)
                    
                    self.mainList.append(newItem)
                    
                }
                
                print("Data downloaded from API... OK")
                
                
                self.stageTable.reloadData()
                self.loadingOverlay.hidden = true
                
                
            case .Failure(let error):
                print("Request Error: \(error)")
            }
        }
        
        
    }

    func networkStatusChanged(notification: NSNotification) {
        let userInfo = notification.userInfo
        print(userInfo)
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "Display Image" {
            let vc = segue.destinationViewController as! DisplayImageViewController
            
            vc.name = currentName
            vc.url = currentUrl
            
        }
        
    }


}


extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mainList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell") as! CustomTableViewCell
        
        let currentItem = mainList[indexPath.row]
        
        cell.name.text = currentItem.name
        cell.desc.text = currentItem.desc
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let currentItem = mainList[indexPath.row]
        
        self.currentUrl = currentItem.cdnUrl
        self.currentName = currentItem.name
        
        performSegueWithIdentifier("Display Image", sender: self)
        
    }
    
    
}


class CustomTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var desc: UILabel!
    
}
