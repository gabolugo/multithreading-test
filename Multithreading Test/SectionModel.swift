//
//  SectionModel.swift
//  Multithreading Test
//
//  Created by Gabo Lugo on 4/8/16.
//  Copyright © 2016 Inspectezr. All rights reserved.
//

import Foundation
import SwiftyJSON

class Section {
    
    let id: String
    let name: String
    let desc: String
    let position: Int
    let cdnUrl: String
    
    init(json: JSON) {
        
        self.id = json["id"].stringValue
        self.name = json["name"].stringValue
        self.desc = json["description"].stringValue
        self.position = json["position"].intValue
        self.cdnUrl = json["cdnUrl"].stringValue
        
    }
    
}