//
//  DisplayImageViewController.swift
//  Multithreading Test
//
//  Created by Gabo Lugo on 4/8/16.
//  Copyright © 2016 Inspectezr. All rights reserved.
//

import UIKit

class DisplayImageViewController: UIViewController {
    
    var url: String = ""
    var name: String = ""
    @IBOutlet weak var canvas: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = name
        
        print(name)
        print(url)
        
        dispatchImage()
        
        
        
    }
    
    func dispatchImage() {
        if let imageData = NSData(contentsOfURL: NSURL(string: url)!) {
            canvas.image = UIImage(data: imageData)
            canvas.contentMode = .ScaleAspectFill
            
            dump(imageData)
            
        } else {
            print("Some error loading the image")
        }
        
        
        
    }


    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
